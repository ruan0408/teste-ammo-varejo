const { Op } = require('sequelize');
const { Product } = require('../models');

module.exports = {
  async searchByTitle(searchText = '') {
    return Product.findAll({
      where: {
        title: { [Op.like]: `%${searchText}%` }
      }
    });
  },
};
