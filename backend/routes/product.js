const express = require('express');
const { product: controller } = require('../controllers');

const router = express.Router();

router.get('/', async (req, res, next) => {
  const { searchText } = req.query;
  const products = await controller.searchByTitle(searchText);
  res.status(200).send(products);
});

module.exports = router;
