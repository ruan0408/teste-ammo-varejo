const data = require('../data');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Product', data);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Product', {});
  }
};
