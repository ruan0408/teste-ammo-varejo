module.exports = function (sequelize, DataTypes) {
  return sequelize.define('Product', {
      title: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      subtitle: {
        type: DataTypes.STRING,
      },
      price: {
        type: DataTypes.DOUBLE,
        allowNull: false,
      },
      image1: {
        type: DataTypes.STRING,
      },
      image2: {
        type: DataTypes.STRING,
      },
      image3: {
        type: DataTypes.STRING,
      },
      image4: {
        type: DataTypes.STRING,
      },
    }, {
      freezeTableName: true,
      paranoid: true,
    },
  );
};

