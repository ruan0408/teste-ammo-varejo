const express = require('express');
const app = express();

const routes = require('./routes');

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', '*');
  res.header('Access-Control-Allow-Headers', '*');
  if (req.mehtod === 'OPTIONS') return res.sendStatus(200);

  next();
});

app.use(express.json());
app.use(routes);
app.use((err, req, res, next) => res.sendStatus(500));

module.exports = app;
