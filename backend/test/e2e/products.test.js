const supertest = require('supertest');
const { expect } = require('chai');

const { sequelize, Product } = require('../../models');
const app = require('../../app');
const request = supertest(app);

describe('/products', () => {
  beforeEach(async () => {
    await sequelize.sync({ force: true });
    await Product.create({ title: 'some title', price: 100 });
  });

  it('returns array and 200 status', () => {
    return request
      .get('/products')
      .expect(200)
      .expect(res => expect(res.body).to.be.an('array'));
  });

  it('returns status 200 and a single element array', () => {
    return request
      .get('/products?searchText=some')
      .expect(200)
      .expect(res => expect(res.body).to.have.length(1));
  });

  it('returns status 200 and empty array', () => {
    return request
      .get('/products?searchText=bla')
      .expect(200)
      .expect(res => expect(res.body).to.be.empty);
  });
});
