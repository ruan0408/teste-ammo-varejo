import React, { Component } from 'react';
import Style from './Style';

import SearchResult from './SearchResult/SearchResult';
import { fetchProducts } from '../API';

import mmartanIcon from '../resources/mmartan-logo.png';
import searchIcon from '../resources/search-icon.svg';
import removeIcon from '../resources/remove-icon.svg';

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      products: null,
      searchText: 'Kit de cama',
      searchedText: '',
    };

    this.handleSearchChange = this.handleSearchChange.bind(this);
    this.handleEnterPress = this.handleEnterPress.bind(this);
    this.handleRemoveClick = this.handleRemoveClick.bind(this);
  }

  async componentDidMount() {
    const products = await fetchProducts(this.state.searchText);
    this.setState({ products, searchedText: this.state.searchText });
  }

  async handleEnterPress(event) {
    if (event.key === 'Enter') {
      const searchText = event.target.value;
      const products = await fetchProducts(searchText);
      this.setState({ products, searchedText: searchText });
    }
  }

  handleSearchChange(e) {
    this.setState({ searchText: e.target.value });
  }

  handleRemoveClick() {
    this.setState({ searchText: '' });
  }

  render() {
    const { searchText, searchedText, products } = this.state;

    if (!products) return null;
    return (
      <Style>
        <div className="header">
          <img src={mmartanIcon} width="100" alt="mmartan"/>
          <div className="div-search">
            <img src={searchIcon} height="11px" alt="search-icon"/>
            <input
              className="search-input"
              type="text"
              value={searchText}
              onChange={this.handleSearchChange}
              onKeyPress={this.handleEnterPress}
            />
            {searchText && (
              <img
                src={removeIcon}
                height="14px"
                alt="remove-icon"
                style={{ cursor: 'pointer' }}
                onClick={this.handleRemoveClick}
              />
            )}
          </div>
        </div>
        <div className="content">
          {searchedText &&
            <div className="search-result-heading">
              <span className="search-text-span">
                {searchedText}
              </span>
            </div>
          }
          <div className="search-result">
            <SearchResult data={products}/>
          </div>
        </div>
      </Style>
    );
  }
}
