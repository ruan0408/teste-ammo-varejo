import styled from 'styled-components';

export default styled.div`
  min-height:  100%;
  display:  flex;
  flex-direction:  column;

  .header {
    height: 50px;
    background-color: whitesmoke;
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 0 20px;
  }
  
  .div-search {
    width: 330px;
    height: 28px;
    display:  flex;
    align-items: center;
    padding: 0 13px;
    border: 2px solid #eae5e5;
    border-radius:  50px;
    background-color:  white;
  }
  
  .search-input { 
    color: darkgray; 
    width: 90%;
    margin-left: 5px;
    border:  none;
    outline:  none;
  }
  
  .content {  
    display:  flex;
    flex-direction:  column;
    flex: 1 1 auto;
  }
  
  .search-result-heading {
    height: 100px;
    background-color: #efeef3;
    padding: 0 20px;
    display: flex;
    align-items: center;    
    border: 2px solid #e2dede;
    border-left: none;
    border-right: none;
  }
  
  .search-text-span {
    font-family: 'Roboto', sans-serif;
    font-size: 31px;
    font-weight: 100;
    color: #473488;
  }
  
  .search-result {
    flex: 1 1 auto;
    padding:  20px 100px;
    background-color: white;
  }
`;
