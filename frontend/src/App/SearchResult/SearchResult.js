import React, { Component } from 'react';

import PaginatedProductList from './PaginatedProductList/PaginatedProductList';
import Style from './Style';

export default class SearchResult extends Component {
  render() {
    const { data } = this.props;
    return (
      <Style>
        <div>
          <div className="products-found-text">
            {data.length > 0 ?
              `${data.length} PRODUTOS ENCONTRADOS` :
              'NENHUM PRODUTO ENCONTRADO'
            }
          </div>
        </div>
        <div className="product-list">
          {data.length > 0 && (
            <PaginatedProductList products={this.props.data}/>
          )}
        </div>
      </Style>
    );
  }
}
