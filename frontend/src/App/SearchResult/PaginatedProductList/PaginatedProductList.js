import React, { Component } from 'react';

import ProductItem from './ProductItem/ProductItem';
import Paginator from './Paginator/Paginator';

export default class PaginatedProductList extends Component {
  constructor() {
    super();
    this.state = {
      rowsPerPage: 16,
      page: 0,
    };

    this.rowsPerPageOptions = [16, 24, 32];
    this.handlePageChange = this.handlePageChange.bind(this);
    this.handleRowsPerPageChange = this.handleRowsPerPageChange.bind(this);
  }

  get offset() {
    const { rowsPerPage, page } = this.state;
    return page * rowsPerPage;
  }

  handlePageChange(newPage) {
    this.setState({ page: newPage })
  }

  handleRowsPerPageChange(event) {
    const newRowsPerPage = parseInt(event.target.value, 10);
    const newPage = Math.floor(this.offset/newRowsPerPage);

    this.setState({
      rowsPerPage: newRowsPerPage,
      page: newPage,
    })
  }

  render() {
    const { products } = this.props;
    const { rowsPerPage, page } = this.state;

    return (
      <div>
        <div>
          {products
            .slice(this.offset, this.offset + rowsPerPage)
            .map((p, index) => (
              <ProductItem
                borderTop={index === 0}
                key={index}
                item={p}
              />
            ))}
        </div>
        <Paginator
          count={products.length}
          rowsPerPage={rowsPerPage}
          rowsPerPageOptions={this.rowsPerPageOptions}
          page={page}
          onRowsPerPageChange={this.handleRowsPerPageChange}
          onPageChange={this.handlePageChange}
        />
      </div>
    );
  }
}
