import React from 'react';
import { SvgIcon } from '@material-ui/core';

export default (props) => (
  <SvgIcon {...props}>
    <path d="M5.59,7.41L10.18,12L5.59,16.59L7,18L13,12L7,6L5.59,7.41M16,6H18V18H16V6Z" />
  </SvgIcon>
)
