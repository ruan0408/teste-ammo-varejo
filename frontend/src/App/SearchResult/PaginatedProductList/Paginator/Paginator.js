import React from 'react';
import { range } from 'lodash';
import { Input, withStyles } from '@material-ui/core';
import Style, {
  FixedPageButton,
  NumberPageButton,
  PageSelector,
  RowSelector,
  RowSelectorItem,
} from './Style';
import NextPageIcon from './NextPageIcon';
import LastPageIcon from './LastPageIcon';
import CheckIcon from './CheckIcon';

const styles = {
  select: {
    background: 'none!important'
  },
};

class Paginator extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      rowSelectorOpen: false,
    };

    this.gotToFirstPage = props.onPageChange.bind(this, 0);
    this.goToPreviousPage = this.incrementPage.bind(this, -1);
    this.goToNextPage = this.incrementPage.bind(this, 1);
  }

  get pageCount() {
    const { count, rowsPerPage } = this.props;
    return Math.floor(count/rowsPerPage) + (count % rowsPerPage === 0 ? 0 : 1);
  }

  incrementPage(inc) {
    const { page, onPageChange } = this.props;
    onPageChange(page + inc);
  }

  hasPreviousPage() {
    return 0 <= this.props.page - 1;
  }

  hasNextPage() {
    return this.props.page + 1 <= (this.pageCount - 1);
  }

  pageButtons() {
    const { page } = this.props;
    const lastPage = this.pageCount - 1;

    let rangeParams = [page - 2, page + 3];
    if (page === 0) rangeParams = [0, 5];
    if (page === 1) rangeParams = [0, 5];
    if (page === lastPage - 1) rangeParams = [lastPage - 4, lastPage + 1];
    if (page === lastPage) rangeParams = [lastPage - 4, lastPage + 1];

    return range(...rangeParams)
      .filter(p => 0 <= p && p <= lastPage);
  }

  render() {
    const {
      rowsPerPage,
      onRowsPerPageChange,
      rowsPerPageOptions,
      onPageChange,
      page,
      classes,
    } = this.props;
    const { rowSelectorOpen } = this.state;
    return (
      <Style>
        <RowSelector
          value={rowsPerPage}
          open={rowSelectorOpen}
          onOpen={() => this.setState({ rowSelectorOpen: true })}
          onClose={() => this.setState({ rowSelectorOpen: false })}
          onChange={onRowsPerPageChange}
          input={<Input disableUnderline />}
          classes={{ select: classes.select }}
        >
          {rowsPerPageOptions.map((n, index) => (
            <RowSelectorItem key={index} value={n}>
              <div className="icon-div">
                {rowSelectorOpen && n === rowsPerPage && (
                  <CheckIcon style={{ fontSize: 15 }}/>
                )}
              </div>
              <div>{n} produtos por página</div>
            </RowSelectorItem>
          ))}
        </RowSelector>
        <PageSelector>
          <FixedPageButton
            disabled={!this.hasPreviousPage()}
            onClick={this.gotToFirstPage}
          >
            <LastPageIcon className="invert-icon"/>
          </FixedPageButton>
          <FixedPageButton
            disabled={!this.hasPreviousPage()}
            onClick={this.goToPreviousPage}
          >
            <NextPageIcon className="invert-icon"/>
          </FixedPageButton>
          {this.pageButtons()
            .map((pageNumber, index) => (
              <NumberPageButton
                key={index}
                selected={pageNumber === page}
                onClick={() => onPageChange(pageNumber)}
              >
                {pageNumber + 1}
              </NumberPageButton>
            ))
          }
          <FixedPageButton
            disabled={!this.hasNextPage()}
            onClick={this.goToNextPage}
          >
            <NextPageIcon style={{ color: '#757373' }}/>
          </FixedPageButton>
          <FixedPageButton
            disabled={!this.hasNextPage()}
            onClick={() => onPageChange(this.pageCount - 1)}
          >
            <LastPageIcon style={{ color: '#757373' }}/>
          </FixedPageButton>
        </PageSelector>
      </Style>
    );
  }
}

export default withStyles(styles)(Paginator);
