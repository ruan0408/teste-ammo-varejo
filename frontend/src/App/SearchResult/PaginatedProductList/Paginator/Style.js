import styled from 'styled-components';
import { Select, MenuItem } from '@material-ui/core';

export default styled.div`
  margin-top: 50px;
  margin-bottom: 50px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  
  .invert-icon {
    color: #757373;
    transform: scaleX(-1);
  }
`;

export const RowSelector = styled(Select)`
  && {
    border: 1px solid lightgrey;
    border-radius: 4px;
    padding: 4px 12px;
    font-size: 14px;
    color: #757373;
    height: 36px;
  }
`;

export const RowSelectorItem = styled(MenuItem)`
  && {
    padding-left: 8px;
    padding-right: 16px;
    color: #757373;
    height: 5px;
  }
  
  .icon-div {
    display: flex;
    align-items: center;
    margin-right: 2px;
    width: 24px;
  }
`;

export const PageSelector = styled.div`
  width: 350px;
  display: flex;
  justify-content: space-between;
`;

export const PaginatorButton = styled.button`
  cursor: pointer;
  height: 40px;
  padding: 0;
  border: none;
  border-radius: 3px;
  background-color: transparent;
  color: #757373;
  outline: none;
  font-size: 13px;
`;

export const NumberPageButton = styled(PaginatorButton)`
  border: ${props => props.selected ? '1px solid #d0caca' : 'none'};
  width: ${props => props.selected ? '40px' : '20px'};
  margin: ${props => props.selected ? '0 5px' : 'initial'};
`;

export const FixedPageButton = styled(PaginatorButton)`
  padding-top: 3px;
  width: 40px;
  cursor: ${props => props.disabled ? 'initial' : 'pointer'};
  opacity: ${props => props.disabled ? 0.2 : 1};
`;
