import React, { Component } from 'react';

import Style from './Style';

export default class ProductItem extends Component {
  render() {
    const { item } = this.props;

    return (
      <Style borderTop={this.props.borderTop}>
        <div>
          <img className="photo" src={item.image1} alt="foto" />
          <img className="photo" src={item.image2} alt="foto"/>
          <img className="photo" src={item.image3} alt="foto"/>
          <img className="photo" src={item.image4} alt="foto"/>
        </div>
        <div className="item-content">
          <div className="item-description">
            <div>
              <div className="item-title">{item.title}</div>
              <div className="item-subtitle">
                {item.subtitle}
              </div>
            </div>
          </div>
          <div className="item-price">
            <span className="discount">R$98,00</span>
            <span className="por"> por </span>
            <span className="price">R${item.price},00</span>
          </div>
        </div>
      </Style>
    );
  }
}
