import styled from 'styled-components';

export default styled.div`
  height: 85px;
  background-color: white;
  display: flex;
  align-items: center;
  margin-top: 1px;
  border: 1px solid #e0e0e0;
  border-top: ${props => props.borderTop ? '' : 'none'};
   
  .photo {
    width: 70px;
    margin: 4px;
  }
  
  .item-content {
    display: flex;
    flex: 2;
    justify-content: space-between;
    padding: 17px;
    align-items: center;
  }
  
  .item-description {
    font-family: Roboto, sans-serif;
    font-weight: 400;
  }
  
  .item-title {
    color: #313131;
  }
  
  .item-subtitle {
    color: darkgray;
    font-size: 12px;
    margin-top: 4px;
  }
  
  .item-price {
    font-weight: 300;
  }
  
  .discount {
    text-decoration: line-through;
    color: darkgray;
  }
  
  .por {
    color: darkgray;
  }
  
  .price {
    color: #464545;
  }
`;