import styled from 'styled-components';

export default styled.div`
  
  .products-found-text {  
    height: 23px;
    display:  inline-block;
    color: grey;
    font-size: 12px;
    border-bottom: 2px solid #ecb749;

  }

  .product-list {
    margin-top: 20px;
  }
`;
