import axios from 'axios';

const apiURL = process.env.REACT_APP_API_URL;

export function fetchProducts(searchText = '') {
  return axios.get(`${apiURL}/products?searchText=${searchText}`)
    .then(res => res.data);
}
